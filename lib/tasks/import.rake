require 'csv'
require 'Nokogiri'
require 'JSON'
require 'httparty'
HTTParty::Basement.default_options.update(verify: false)

namespace :import do 
  desc "Import localities from csv"
  task localities: :environment do 
    
# this is to request the page we are going to scrape
page = HTTParty.get('https://www.listofcitynames.com/India/ViewAll?City=True&Village=True&Suburb=True&Town=True&Neighbourhood=True')

document = Nokogiri::HTML(page)

csv_file_path = File.join(Rails.root, "localities.csv")
CSV.open(csv_file_path, "wb") do |csv|
  document.at('table').search('tr').each do |row|
    csv << row.search('th, td').map(&:text)
  end
end

csv = CSV.read(csv_file_path, headers: true)
csv.delete("Type") # Remove the entire column with heading "Type"

File.open(csv_file_path, "w") do |file|
  file.write(csv.to_csv)
end

Locality.import(csv_file_path)
  end
end 