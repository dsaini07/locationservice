class AddNotNullAndUniquenessToCity < ActiveRecord::Migration[5.1]
  def change
  change_column :cities, :name, :string, null: false
  add_index :cities, :name, unique: true
  end
end