class AddNotNullToLocality < ActiveRecord::Migration[5.1]
  def change
  change_column :localities, :name, :string, null: false
  change_column :localities, :longitude, :decimal, null: false
  change_column :localities, :latitude, :decimal, null: false
  end
end