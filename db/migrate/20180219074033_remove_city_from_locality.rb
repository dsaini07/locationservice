class RemoveCityFromLocality < ActiveRecord::Migration[5.1]
  def change
    remove_column :localities, :city, :string
  end
end
