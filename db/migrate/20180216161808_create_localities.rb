class CreateLocalities < ActiveRecord::Migration[5.1]
  def change
    create_table :localities do |t|
      t.string :name
      t.string :city
      t.decimal :latitude
      t.decimal :longitude
      t.integer :pincode

      t.timestamps
    end
  end
end
