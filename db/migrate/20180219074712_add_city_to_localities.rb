class AddCityToLocalities < ActiveRecord::Migration[5.1]
  def change
    add_reference :localities, :city, foreign_key: true
  end
end
