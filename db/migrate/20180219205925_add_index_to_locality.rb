class AddIndexToLocality < ActiveRecord::Migration[5.1]
  def change
  add_index :localities, [:latitude, :longitude], unique: true
  end
end