Rails.application.routes.draw do
  resources :cities do
      resources :localities
      end
      
    get 'localities', to: 'localities#index', as: :custom_search
end