require 'HTTParty'
require 'Nokogiri'
require 'JSON'
require 'csv'

HTTParty::Basement.default_options.update(verify: false)

# this is to request the page we are going to scrape
page = HTTParty.get('https://www.listofcitynames.com/India/ViewAll?City=True&Village=True&Suburb=True&Town=True&Neighbourhood=True')

document = Nokogiri::HTML(page)

CSV.open("localities.csv", "wb") do |csv|
  document.at('table').search('tr').each do |row|
    csv << row.search('th, td').map(&:text)
  end
end

csv = CSV.read("localities.csv",{headers: true, return_headers: true})
csv.delete("Type")

File.open("localities.csv", "w") do |file|
  file.write(csv.to_csv)
end