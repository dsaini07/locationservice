FactoryBot.define do
  
  factory :locality do
    name "Hansi"
    pincode 123456
    city
    latitude 123.456
    longitude 789.123
  end

  factory :invalid_locality, parent: :locality do
  name nil
  end

  factory :city do
    sequence(:name) { |n| "Jaipur1" + n.to_s }
	
    # city_with_localities will create locality data after the city has been created
    factory :city_with_localities do
      # localities_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        localities_count 5
      end

      # the after(:create) yields two values; the city instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the city is associated properly to the locality
      after(:create) do |city, evaluator|
        create_list(:locality, evaluator.localities_count, city: city)
      end
    end
  end
end