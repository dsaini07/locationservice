require "rails_helper"
RSpec.describe Locality, :type => :model do
  
  before(:all) do
    @locality1 = create(:locality)
  end
  
  it "is valid with valid attributes" do
    expect(@locality1).to be_valid
  end
  
  it "has a unique longitude" do
    locality2 = build(:locality, longitude: @locality1.longitude)
    expect(locality2).to_not be_valid
  end

  it "has a unique latitude" do
    locality2 = build(:locality, latitude: @locality1.latitude)
    expect(locality2).to_not be_valid
  end
  
  it "is not valid without a name" do 
    locality2 = build(:locality, name: nil)
    expect(locality2).to_not be_valid
  end
  
  it "is not valid without a latitude" do 
    locality2 = build(:locality, latitude: nil)
    expect(locality2).to_not be_valid
  end
  
  it "is not valid without a longitude" do
    locality2 = build(:locality, longitude: nil)
    expect(locality2).to_not be_valid
  end

  it "is not valid without a city" do
    locality2 = build(:locality, city: nil)
    expect(locality2).to_not be_valid
  end

  it "should have minimum 3 characters in name" do 
    locality2 = build(:locality, name: "ab")
    expect(locality2).to_not be_valid
  end
end