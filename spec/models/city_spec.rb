require "rails_helper"
RSpec.describe City, :type => :model do
  
  before(:all) do
    @city1 = create(:city)
  end
  
  it "is valid with valid attributes" do
    expect(@city1).to be_valid
  end
  
  it "has a unique name" do
    city2 = build(:city, name: @city1.name)
    expect(city2).to_not be_valid
  end
  
  it "is not valid without a name" do 
    city2 = build(:city, name: nil)
    expect(city2).to_not be_valid
  end

  it "should have minimum 3 characters in name" do 
    city2 = build(:city, name: "ab")
    expect(city2).to_not be_valid
  end

  
  
end