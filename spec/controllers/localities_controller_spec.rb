require "rails_helper"
RSpec.describe LocalitiesController, :type => :controller do
  describe "GET #index" do
  before(:each) do
    @city = create(:city)
  end
  it "populates an array of localities" do 
    locality = create(:locality, city: @city)
    get :index, params: {city_id: @city.id}
    expect(assigns(:localities)).to eq([locality])
  end
  
  it "renders the :index view" do
    get :index, params: {city_id: @city.id}
    expect(response).to render_template("index")
  end
end

describe "GET #show" do
  before(:each) do
    @city = create(:city)
  end
  
  it "assigns the requested locality to @locality" do
    locality = create(:locality, city: @city)
    get :show, params: {city_id: @city.id, id: locality.id}
    expect(assigns(:locality)).to eq(locality)
  end
  
  it "renders the #show view" do
    locality = create(:locality, city: @city)
    get :show, params: {city_id: @city.id, id: locality.id}
    expect(response).to render_template("show")
  end
end

describe "POST create" do
  before(:each) do
    @city = create(:city)
  end
  
  context "with valid attributes" do
    it "creates a new locality" do
      expect{
        post :create, params: {city_id: @city.id, locality: attributes_for(:locality, city: @city)}
      }.to change(Locality,:count).by(1)
    end
    
    it "redirects to the new locality" do
      post :create, params: {city_id: @city.id, locality: attributes_for(:locality, city: @city)}
      expect(response).to redirect_to([@city, Locality.last])
    end
  end
  
  context "with invalid attributes" do
    it "does not save the new locality" do
      expect{
        post :create, params: {city_id: @city.id, locality: attributes_for(:invalid_locality)}
      }.to_not change(Locality,:count)
    end
    
    it "re-renders the new method" do
      post :create, params: {city_id: @city.id, locality: attributes_for(:invalid_locality)}
      expect(response).to render_template("new")
    end
  end 
end

describe 'DELETE destroy' do
  before :each do
    @city = create(:city)
    @locality = create(:locality, city: @city)
  end
  
  it "deletes the locality" do
    expect{
      delete :destroy, params: {city_id: @city.id, id: @locality.id}        
    }.to change(Locality,:count).by(-1)
  end
    
  it "redirects to localities#index" do
    delete :destroy, params: {city_id: @city.id, id: @locality.id}
    expect(response).to redirect_to city_localities_url
  end
end

describe 'PUT update' do
  before :each do
    @city = create(:city)
    @locality = create(:locality, city: @city)
  end
  
  context "valid attributes" do
    it "located the requested @locality" do
      put :update, params: {city_id: @city.id, id: @locality.id, locality: attributes_for(:locality, city: @city)}
      expect(assigns(:locality)).to eq(@locality)
    end
  
    it "changes @locality's attributes" do
      attr = attributes_for(:locality, name: "abcxyz", city: @city)
      put :update, params: {city_id: @city.id, id: @locality.id, locality: attr}
      @locality.reload
      expect(@locality.name).to eq(attr[:name])
    end
  
    it "redirects to the updated locality" do
     put :update, params: {city_id: @city.id, id: @locality.id, locality: attributes_for(:locality, city: @city)}
      expect(response).to redirect_to([@city, @locality])      
    end
  end
  
  context "invalid attributes" do
    it "locates the requested @contact" do
     put :update, params: {city_id: @city.id, id: @locality.id, locality: attributes_for(:locality, city: @city, name: nil)}
      expect(assigns(:locality)).to eq(@locality)      
    end
    
    it "does not change @locality's attributes" do
    attr = attributes_for(:locality, name: "abcxyz", latitude: nil, city: @city)
      put :update, params: {city_id: @city.id, id: @locality.id, locality: attr}
      @locality.reload
      expect(@locality.name).to_not eq(attr[:name])
    end
    
    it "re-renders the edit method" do
     put :update, params: {city_id: @city.id, id: @locality.id, locality: attributes_for(:locality, name: "abcxyz", latitude: nil, city: @city)}
     expect(response).to render_template("edit")
    end
  end
end
end