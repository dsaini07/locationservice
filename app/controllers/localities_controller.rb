class LocalitiesController < ApplicationController
  before_action :get_city, except: :index
  before_action :set_locality, only: [:show, :edit, :update, :destroy]
  
  # GET /cities/1/localities
  # GET /cities/1/localities.json
  def index
    if params[:city_id]
        @city = get_city
        @localities = @city.localities
    end
    
  # This will help us return a locality for given vales of Latitude and Longitude
    if longitude = params[:longitude] and latitude = params[:latitude]
        @localities = Locality.where({longitude: longitude, latitude: latitude})
    end
 
  # This will help us return localities matching given name
    if name = params[:name]
        @localities = Locality.where("name LIKE ?", "%#{name}%")
    end
  end

  # GET /cities/1/localities/1
  # GET /cities/1/localities/1.json
  def show
      @locality = @city.localities.find(params[:id])
  end

  # GET /cities/1/localities/new
  def new
    @locality = @city.localities.build
  end

  # GET /cities/1/localities/1/edit
  def edit
      @locality = @city.localities.find(params[:id])
  end

  # POST /cities/1/localities
  # POST /cities/1/localities.json
  def create
    
    @locality = @city.localities.create(locality_params)
    
    respond_to do |format|
      if @locality.save
        format.html { redirect_to [@city, @locality], notice: 'Locality was successfully created.' }
        format.json { render :show, status: :created, location: [@city, @locality] }
      else
        format.html { render :new }
        format.json { render json: @locality.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1/localities/1
  # PATCH/PUT /cities/1/localities/1.json
  def update
    @locality = @city.localities.find(params[:id])
    respond_to do |format|
      if @locality.update(locality_params)
        format.html { redirect_to [@city, @locality], notice: 'Locality was successfully updated.' }
        format.json { render :show, status: :ok, location: [@city, @locality] }
      else
        format.html { render :edit }
        format.json { render json: @locality.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cities/1/localities/1
  # DELETE /cities/1/localities/1.json
  def destroy
    @locality.destroy
    respond_to do |format|
      format.html { redirect_to city_localities_url, notice: 'Locality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_locality
      @locality = @city.localities.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def locality_params
      params.require(:locality).permit(:name, :city_id, :latitude, :longitude, :pincode)
    end
    
    def get_city
        @city = City.find(params[:city_id])        
    end
    
end