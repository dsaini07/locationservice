class City < ApplicationRecord
has_many :localities
validates :name, presence: true, uniqueness: true, length: { minimum: 3 }
end