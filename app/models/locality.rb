class Locality < ApplicationRecord
belongs_to :city
validates :name, :city, :latitude, :longitude, presence: true
validates :name, length: { minimum: 3 }
validates :longitude, :latitude, uniqueness: true
validates_associated :city
    
    require 'csv'
    
    def self.import(filepath)
        CSV.foreach(filepath, headers: ['name', 'city', 'latitude', 'longitude']) do |row|
        
            row_hash = row.to_hash
            Locality.create! row_hash.merge(city: City.find_or_create_by(name: row_hash['city']))
            end
    end
end