json.extract! city, :id, :created_at, :updated_at, :name
json.url city_url(city, format: :json)
