json.extract! locality, :id, :created_at, :updated_at, :name, :latitude, :longitude
json.url city_localities_url(locality, format: :json)